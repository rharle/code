import json
import sys
import re
from datetime import timedelta, datetime
from os import environ


setting_file = "settings.json"

shebang_string = "#!/bin/bash\n"
empty_line = "#\n"
disclaimer_string = "# This file was generated automatically by the start script based on the settings.json file\n"

def export_env_var(var):
  return f"""{var}=\"{environ.get(var, "")}\"
export {var}
""" + "\n"

def validate_queue(queue):
  if not queue in ["dedicated", "default", "big", "besteffort"]:
    sys.exit("Error: Invalid queue name")

def format_timedelta(td):
  seconds = td.total_seconds()
  (h, seconds) = divmod(seconds, 3600)
  (m, s) = divmod(seconds, 60)

  return f"{int(h)}:{int(m)}:{int(s)}"

def validate_time(time):
  hms_re = re.compile(r'(\d+):(\d+):(\d+)')
  hm_re = re.compile(r'(\d+):(\d+)')
  h_re = re.compile(r'(\d+)')

  h = 0
  m = 0
  s = 0

  match = hms_re.match(time)
  if match is not None:
    h = int(match.group(1))
    m = int(match.group(2))
    s = int(match.group(3))

  match = hm_re.match(time)
  if match is not None:
    h = int(match.group(1))
    m = int(match.group(2))

  match = h_re.match(time)
  if match is not None:
    h = int(match.group(1))

  if (h == 0) & (m == 0) & (s == 0):
    sys.exit("Error in validate_time: invalid or null walltime")

  return timedelta(hours = h, minutes = m, seconds = s)


def generate_oar_proc_string(nodes, cores, time):
  if not isinstance(time, timedelta):
    sys.exit("Error in generate_oar_proc_string : no time limit provided")
  if (nodes is None) & (cores is None):
    sys.exit("Error in generate_oar_proc_string : no nodes or core number")

  if nodes is None:
    comment = f"# The job reserves {cores} processors (cores) possibly splitted accross nodes"
  elif cores is  None:
    comment = f"# The job reserves {nodes} full nodes"
  else:
    comment = f"# The job reserves {nodes} nodes with {cores} processor (core) per node"
  comment += "\n" + f"# Maximum job duration is {time}" + "\n"

  oar_string = "#OAR -l "
  if nodes is not None:
    nodes = int(nodes)
    oar_string += f"/nodes={nodes}"
  if cores is not None:
    cores = int(cores)
    oar_string += f"/core={cores}"

  oar_string += f",walltime={format_timedelta(time)}" + "\n"

  return comment + oar_string

if __name__ == "__main__":
  with open(setting_file, "r") as file:
    settings_dict = json.load(file)

  dir_settings = settings_dict.get("dir", {})
  oar = settings_dict.get("OAR", {})
  nodes = oar.get("nodes", None)
  cores = oar.get("cores", None)
  queue = oar.get("queue", "default")
  validate_queue(queue)
  walltime = validate_time(oar.get("walltime", ""))
  parallel_method = environ.get("PARALLEL_METHOD")
  job_name = environ.get("ANALYSIS_NAME")

  extra_params = oar.get("p", None)
  if extra_params is not None:
    oar_extra_string = f"#OAR -p {extra_params}" + "\n"
  else:
    oar_extra_string = ""

  oar_queue_string = f"""# The job is submitted to the {queue} queue
#OAR -q {queue}
"""
  oar_proc_string = generate_oar_proc_string(nodes, cores, walltime)

  setup_string = """# Load Open MPI
source /etc/profile.d/modules.sh
module load mpi/intel64-5.1.1.109
module load gcc/9.2.0

# Set environment variables
OAR_CORES=`wc -l < $OAR_NODEFILE`
export OAR_CORES

"""
  if parallel_method == "MPI":
    exec_string = """# Run the R entry point
mpirun -machinefile $OAR_NODEFILE  -np 1 Rscript .nef/script_entry_point.R
"""
  else:
    exec_string = """# Run the R entry point
Rscript .nef/script_entry_point.R
"""
    
  script_name = datetime.today().strftime('%Y_%m_%d_%H:%M:%S') + '_' + job_name + '_submission.sh'
  with open(script_name, "w") as file:
    file.write(shebang_string)
    file.write(empty_line)
    file.write(disclaimer_string)
    file.write(empty_line)
    file.write(oar_proc_string)
    file.write(oar_queue_string)
    file.write(oar_extra_string)
    file.write(setup_string)
    file.write(export_env_var("ANALYSIS_NAME"))
    file.write(export_env_var("SRC_DIR"))
    file.write(export_env_var("DATA_DIR"))
    file.write(export_env_var("OUTPUT_DIR"))
    file.write(export_env_var("MAIN_R_SCRIPT"))
    file.write(export_env_var("PARALLEL_METHOD"))
    file.write(export_env_var("R_LIB_PATH"))
    file.write(export_env_var("RETICULATE_ENV_NAME"))
    file.write(exec_string)


  print(script_name)

