# This script read a setting file in json format and return the value of the
# key passed as an argument.
# Sub keys are handelled through dot syntax e.g.: key.subkey
# Access to list element by position (starting at 1) is also valid
# If the requested key is invalid, the output is a blank string
# and return code is -1

import sys
import json

setting_file = "settings.json"


if __name__ == "__main__":
  # verify that the script was called with a single argument
  if len(sys.argv) != 2:
    sys.exit("Error: Wrong usage, this script must be called with a unique argument")

  # get the setting name as a list of keys
  keys_list = sys.argv[1].split('.')

  with open(setting_file, "r") as file:
    settings_dict = json.load(file)

  current_dict = settings_dict

  for key in keys_list:
    try:
      if isinstance(current_dict, list):
        key = int(key) - 1
      current_dict = current_dict[key]
    # if something went wrong, print nothing, default value will be handeled
    # by the downstream sh script
    except (ValueError, IndexError, KeyError):
      print('', end='')
      sys.exit(-1)

  # pretty printing of lists
  if isinstance(current_dict, list):
    try:
      concat_string = ""
      for item in current_dict:
        concat_string += str(item) + ' '
      current_dict = concat_string[0:-1]
    except IndexError:
      print('', end='')
      sys.exit(-1)

  # print the fetched key
  print(current_dict, end='')


