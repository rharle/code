# Populate shell variables with settings
OAR_CORES=`python3 .nef/parse_settings.py OAR.cores`
[[ -z "$OAR_CORES" ]] && OAR_CORES=1;
OAR_NODES=`python3 .nef/parse_settings.py OAR.nodes`
[[ -z "$OAR_NODES" ]] && OAR_NODES=1;
ANALYSIS_NAME=`python3 .nef/parse_settings.py analysis_name`
[[ -z "$ANALYSIS_NAME" ]] && ANALYSIS_NAME=unnamed_analysis;
SRC_DIR=`python3 .nef/parse_settings.py dir.src`
[[ -z "$SRC_DIR" ]] && SRC_DIR=src/;
DATA_DIR=`python3 .nef/parse_settings.py dir.data`
[[ -z "$DATA_DIR" ]] && DATA_DIR=data/;
OUTPUT_DIR=`python3 .nef/parse_settings.py dir.out`
[[ -z "$OUTPUT_DIR" ]] && OUTPUT_DIR=output/;
MAIN_R_SCRIPT=`python3 .nef/parse_settings.py r_script`
[[ -z "$MAIN_R_SCRIPT" ]] && MAIN_R_SCRIPT=main.R;
PARALLEL_METHOD=`python3 .nef/parse_settings.py parallel`
[[ -z "$PARALLEL_METHOD" ]] && PARALLEL_METHOD="fork";
LOG_FILE=`python3 .nef/parse_settings.py logfile`
[[ -z "$LOG_FILE" ]] && LOG_FILE=install.log;
R_LIB_PATH=`python3 .nef/parse_settings.py dir.lib`
[[ -z "$R_LIB_PATH" ]] && R_LIB_PATH=`Rscript -e "cat(Sys.getenv('R_LIBS_USER'))"`;
PY_ENV_NAME=`python3 .nef/parse_settings.py py_env`
[[ -z "$PY_ENV_NAME" ]] && PY_ENV_NAME=base;
RETICULATE_ENV_NAME=`python3 .nef/parse_settings.py reticulate_env`


# export variables for later use
export SUBMISSION_SCRIPT
export OAR_CORES
export OAR_NODES
export ANALYSIS_NAME
export SRC_DIR
export DATA_DIR
export OUTPUT_DIR
export MAIN_R_SCRIPT
export R_LIB_PATH
export PY_ENV_NAME
export RETICULATE_ENV_NAME
export PARALLEL_METHOD
export LOG_FILE