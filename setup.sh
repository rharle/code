#colors
RED=[31m
BLUE=[34m
RESET=[0m
LOG_FILE=setup.log


# create dir for R packages
if [ -z  "$R_PACKAGE_DIR" ]
  then
  echo -e "\e${BLUE}INFO:\e${RESET} No specific R lib directory specified, using the R default"
  R_PACKAGE_DIR=`Rscript -e 'cat(normalizePath(Sys.getenv("R_LIBS_USER"), mustWork = FALSE))'`
else
  R_LIBS_USER=$R_PACKAGE_DIR
  export R_LIBS_USER
  echo -e "\e${BLUE}INFO:\e${RESET} Using requested R lib dir, $R_PACKAGE_DIR"
fi

# create R package dir if it does not exists
if [ ! -d "$R_PACKAGE_DIR" ]; then
  echo -e "\e${BLUE}INFO:\e${RESET} Creating R package directory: $R_PACKAGE_DIR"
  mkdir -p $R_PACKAGE_DIR
fi



# Install tidyverse from cran
Rscript -e 'install.packages("tidyverse", repos="https://cloud.r-project.org")' 2>> $LOG_FILE
if [ $? -eq 0 ]; then
  echo -e "\e${RED}Error: Something went wrong during the installation of some R package.\e${RESET}
    Please read $LOG_FILE for more details"
fi

# Install parallellization packages from cran
Rscript -e 'install.packages(c("snow", "parallelly", "foreach", "doFuture"), repos="https://cloud.r-project.org")'

# Install reticulate from cran
Rscript -e 'install.packages("reticulate", repos="https://cloud.r-project.org")'


